export default (total: number, coins: number[] = [1, 2, 5]): number[] => {
  //
  if (typeof total !== 'number') {
    throw new Error('Total amount param is missing or invalid');
  }

  if (!Array.isArray(coins) || !coins.every((item) => typeof item === 'number')) {
    throw new Error('The coins param should be an array of numbers');
  }

  // Copy the total amount to subtract the found coins value
  let remaining = total;

  // Avoid repetitions on the coins by using a temporary set
  const setCoins = new Set(coins);

  // Sort coins from bigger to smaller. That way we can try to cover first the biggest amount of money
  // with the biggest coins. As a result we will have a small number of coins.
  return Array.from(setCoins).sort((a, b) => b - a).reduce((changeReturn: number[], currentCoin: number) => {
    // Calculate the number of coins
    const numberOfCoins = Math.floor(remaining / currentCoin);

    // Subtract the amount
    remaining -= numberOfCoins * currentCoin;

    // Concat the new coins to the previous one added.
    return changeReturn.concat(Array(numberOfCoins).fill(currentCoin));
  }, []);
}