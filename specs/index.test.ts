import { expect } from 'chai';
import changeReturn from '../src/index';

describe('Change return', function () {
  it('should return the correct change', () => {
    const returnCoins = changeReturn(12);
    const resultShouldBe = [5, 5, 2];

    expect(returnCoins).to.be.an('array');
    expect(returnCoins.length).to.equal(resultShouldBe.length);
    expect(returnCoins).to.eql(resultShouldBe);
  });

  it('should calculate correctly the change with a different list of coins', () => {
    const returnCoins = changeReturn(28, [1, 2, 5, 10]);
    const resultShouldBe = [10, 10, 5, 2, 1];

    expect(returnCoins).to.eql(resultShouldBe);
  });

  it('should calculate correctly the change passing an unordered list of coins', () => {
    const returnCoins = changeReturn(28, [2, 10, 5, 1]);
    const resultShouldBe = [10, 10, 5, 2, 1];

    expect(returnCoins).to.eql(resultShouldBe);
  });

  it('should calculate correctly the change passing an unordered and repeated list of coins', () => {
    const returnCoins = changeReturn(28, [2, 10, 5, 1, 1, 10, 5, 2, 2]);
    const resultShouldBe = [10, 10, 5, 2, 1];

    expect(returnCoins).to.eql(resultShouldBe);
  });

  it('throw an error when total param is not a number', () => {
    // @ts-ignore
    expect(changeReturn.bind(null, '30')).to.throw('Total amount param is missing or invalid');
  });

  it('throw an error when the coins param is not an array', () => {
    // @ts-ignore
    expect(changeReturn.bind(null, 28, null)).to.throw('The coins param should be an array of numbers');
    // @ts-ignore
    expect(changeReturn.bind(null, 28, '40')).to.throw('The coins param should be an array of numbers');
  });

  it('throw an error when the coins param contains an item which is not a number', () => {
    // @ts-ignore
    expect(changeReturn.bind(null, 28, [1, 2, 3, '30'])).to.throw('The coins param should be an array of numbers');
  });
});