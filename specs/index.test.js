"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const index_1 = __importDefault(require("../src/index"));
describe('Change return', function () {
    it('should return the correct change', () => {
        const returnCoins = (0, index_1.default)(12);
        const resultShouldBe = [5, 5, 2];
        (0, chai_1.expect)(returnCoins).to.be.an('array');
        (0, chai_1.expect)(returnCoins.length).to.equal(resultShouldBe.length);
        (0, chai_1.expect)(returnCoins).to.eql(resultShouldBe);
    });
    it('should calculate correctly the change with a different list of coins', () => {
        const returnCoins = (0, index_1.default)(28, [1, 2, 5, 10]);
        const resultShouldBe = [10, 10, 5, 2, 1];
        (0, chai_1.expect)(returnCoins).to.eql(resultShouldBe);
    });
    it('should calculate correctly the change passing an unordered list of coins', () => {
        const returnCoins = (0, index_1.default)(28, [2, 10, 5, 1]);
        const resultShouldBe = [10, 10, 5, 2, 1];
        (0, chai_1.expect)(returnCoins).to.eql(resultShouldBe);
    });
});
